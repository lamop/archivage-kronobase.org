Archivage sous la forme d'un fichier CSV du site http://www.kronobase.org pour la
partie Moyen Âge

L'objectif est de garder une sauvegarde du site http://www.kronobase.org sous
une forme simple (CSV)

contact : archive@lamop.fr

Tous les textes sont disponibles sous les termes de la licence de documentation
libre GNU (http://www.gnu.org/licenses/fdl-1.3.html)
KronoBase® est une marque déposée par l'Association KronoBase loi 1901, depuis 2006
-->> http://www.kronobase.org/ <<--

Notes :
mise en ligne : 07-05-2020
